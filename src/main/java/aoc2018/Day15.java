package aoc2018;

import aoc2018.day15.*;

import lombok.*;

class Day15 extends Solver {
    @Getter private final CombatMap combatMap;
    @Getter private final Control control;

    Day15(String file) {
        setDataFile(file);
        this.combatMap = CombatMap.parseCombatMap(getLines());
        this.control = new Control(combatMap);
    }

    long call1() {
        control.run();
        return control.getRound();
    }

    String[] map() {
        return combatMap.displayMap();
    }
}
