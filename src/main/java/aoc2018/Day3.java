package aoc2018;

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import java.util.function.Function;
import java.util.stream.IntStream;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import lombok.*;

class Day3 extends Solver {
    @Builder(builderClassName="Builder")
    private static class Claim {
        private final int id;
        private final int row;
        private final int column;
        private final int rowSize;
        private final int columnSize;
    }

    private final List<Claim> claims;
    private final int[][] plane;

    Day3(String file) {
        setDataFile(file);

        this.claims = parseClaims();
        this.plane = colorPlane(makePlane());
    }

    public long call1() {
        return countOverlapping();
    }

    public int call2() {
        return claims.stream()
            .filter(claim -> !isOverlapping(claim))
            .findFirst()
            .get()
            .id;
    }

    // private
    private boolean isOverlapping(Claim claim) {
        for (int r = 0; r < claim.rowSize; r++)
            for (int c = 0; c < claim.columnSize; c++)
                if (plane[claim.row + r][claim.column + c] > 1) return true;

        return false;
    }

    private long countOverlapping() {
        return Arrays
            .stream(plane)
            .flatMapToInt(r -> Arrays.stream(r))
            .filter(e -> e > 1)
            .count();
    }

    private List<Claim> parseClaims() {
        val claims = new ArrayList<Claim>();
        val pattern = Pattern.compile("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)");

        for (val line : getLines()) {
            val matcher = pattern.matcher(line);

            if (matcher.find()) {
                Function<Integer, Integer> f = i -> Integer
                    .parseInt(matcher.group(i));

                claims.add(Claim.builder()
                           .id(f.apply(1))
                           .column(f.apply(2))
                           .row(f.apply(3))
                           .columnSize(f.apply(4))
                           .rowSize(f.apply(5))
                           .build());
            } else {
                throw new IllegalArgumentException(line + " is not correct");
            }
        }

        return claims;
    }

    private int[][] makePlane() {
        int maxRow = claims.stream().mapToInt(c -> c.row + c.rowSize)
            .max().getAsInt();
        int maxColumn = claims.stream().mapToInt(c -> c.column + c.columnSize)
            .max().getAsInt();

        return new int[++maxRow][++maxColumn];
    }

    private int[][] colorPlane(int[][] plane) {
        for (val claim : claims) {
            for (int r = 0; r < claim.rowSize; r++)
                for (int c = 0; c < claim.columnSize; c++)
                    plane[claim.row + r][claim.column + c]++;
        }
        return plane;
    }
}
