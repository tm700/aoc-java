package aoc2018.day15;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

import lombok.*;

/**
 *
 * @author marty.ma
 */
class Unit {
    @Getter private final char unitType;
    @Getter private int hitPoints = 200;
    private int attackPower = 3;

    Unit(int row, int column, char unitType) {
        if (unitType != Point.ELF && unitType != Point.GOBLIN) {
            throw new IllegalArgumentException("Not accepted unit type: " + unitType);
        }
        this.unitType = unitType;
    }

    @Override
    public String toString() {
        return unitType + " " + hitPoints;
    }

    boolean isGoblin() { return unitType == Point.GOBLIN; }

    boolean isElf() { return unitType == Point.ELF; }

    boolean isEnemy(Unit unit) {
        requireNonNull(unit);

        return unit.unitType != this.unitType;
    }

    boolean isFriend(Unit unit) { return unit.unitType == this.unitType; }

    void attack(Unit enemy) {
        if (!this.isEnemy(enemy)) {
            throw new IllegalArgumentException("Only allow to attack enemy");
        }

        enemy.hit(attackPower);
    }

    private void hit(int attackPower) {
        if (isAlive()) this.hitPoints -= attackPower;
    }

    boolean isDead() {
        return this.hitPoints <= 0;
    }

    boolean isAlive() {
        return this.hitPoints > 0;
    }
}
