package aoc2018.day15;

import lombok.*;

public class Point {
    static char SPACE  = '.';
    static char WALL   = '#';
    static char ELF    = 'E';
    static char GOBLIN = 'G';

    @Getter(AccessLevel.PACKAGE) private final int row;
    @Getter(AccessLevel.PACKAGE) private final int column;
    @Getter(AccessLevel.PACKAGE) private final int readingOrder;
    @Getter(AccessLevel.PACKAGE) private boolean isSpace = true;
    @Getter(AccessLevel.PACKAGE) private boolean isWall = false;
    @Getter(AccessLevel.PACKAGE) private Unit unit;

    Point(int row, int column, int readingOrder) {
        this.row          = row;
        this.column       = column;
        this.readingOrder = readingOrder;
        setSpace();
    }

    void setSpace() {
        isSpace = true;
        isWall  = false;
        unit  = null;
    }

    void setWall() {
        this.isWall  = true;
        this.isSpace = false;
        this.unit    = null;
    }

    void setUnit(Unit unit) {
        this.unit    = unit;
        this.isSpace = false;
        this.isWall  = false;;
    }

    boolean isUnit()  { return this.unit != null; }

    String getSymbol() {
        if (isSpace()) return ".";
        if (isWall()) return "#";
        if (isUnit()) {
            if (unit.isElf()) return "E";
        }

        return "G";
    }

    @Override
    public String toString() {
        var s = row + "," + column;

        if (isUnit()) {
            s += ", " + unit.toString();
        } else if (isSpace()) {
            s += ", " + ".";
        } else {
            s += ", " + "#";
        }

        return s;
    }

    private void clear() {
    }
}
