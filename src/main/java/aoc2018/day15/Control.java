package aoc2018.day15;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import lombok.*;

public class Control {
    @Getter private int round = 0;

    private final CombatMap combatMap;
    private final Navigator navigator;

    private final List<Unit> roundUnits = new ArrayList<Unit>();

    public Control(CombatMap combatMap) {
        this.combatMap = combatMap;
        this.navigator = new Navigator(combatMap);
    }

    public void run() {
        boolean stop = false;

        while (doRound());
    }

    public void printUnitPoints(List<Unit> units) {
        System.err.println("\n");
        for (val unit : units) {
            System.err.println(
                unit.getUnitType() + "(" + unit.getHitPoints() + ")"
                );
        }

        System.err.println("\n\n");
    }

    public int getTotalHitPoints() {
        int sum = 0;
        for (int row = 0; row < combatMap.getRowSize(); row++) {
            for (int column = 0; column < combatMap.getColumnSize(); column++) {
                val p = combatMap.getPoint(row, column);
                if (p.isUnit())
                    sum += p.getUnit().getHitPoints();
            }
        }

        return sum;
    }

    public List<Point> getPoints() {
        var points = new ArrayList<Point>();

        for (int row = 0; row < combatMap.getRowSize(); row++) {
            for (int column = 0; column < combatMap.getColumnSize(); column++) {
                val p = combatMap.getPoint(row, column);

                if (p.isUnit()) points.add(p);
            }
        }

        return points;
    }

    public boolean doRound() {
        roundUnits.clear();
        val points = getPoints();

        for (val p : points) {
            roundUnits.add(p.getUnit());
        }

        if (points.stream().anyMatch(p -> p.getUnit().isElf())
            && points.stream().anyMatch(p -> p.getUnit().isGoblin())) {
            round++;

            for (val p : points)
                doPoint(p);

            System.err.println("Round: " + getRound());

            for (val line : combatMap.displayMap()) {
                System.err.println(line);
            }

            printUnitPoints(roundUnits);

            return true;
        }

        System.err.println("Round: " + getRound());

        for (val line : combatMap.displayMap()) {
            System.err.println(line);
        }

        printUnitPoints(roundUnits);

        return false;
    }

    private void doPoint(Point point) {
        if (!point.isUnit()) return;

        var enemyPoint = findAdjacentEnemyPoint(point);

        if (isNull(enemyPoint)) {
            point = move(point);
            if (isNull(point)) return;
            enemyPoint = findAdjacentEnemyPoint(point);
        }

        if (nonNull(enemyPoint))
            attack(point, enemyPoint);

        // val unit = point.getUnit();
        // afterRoundPoints.add(point);
    }

    private Point move(Point point) {
        val target = navigator.moveTarget(point);

        if (nonNull(target))
            combatMap.movePoint(point, target);

        return target;
    }

    private void attack(Point point, Point enemyPoint) {
        val enemy = enemyPoint.getUnit();
        point.getUnit().attack(enemy);

        if (enemy.isDead()) enemyPoint.setSpace();
    }

    private Point findAdjacentEnemyPoint(Point point) {
        val surroundingEnermies = combatMap.getSurroundingPoints(point)
            .filter(sp -> sp.isUnit())
            .filter(sp -> sp.getUnit().isEnemy(point.getUnit()))
            .min((a, b) ->
                 {
                     int compared = a.getUnit().getHitPoints() - b.getUnit().getHitPoints();
                     return compared == 0 ?
                         a.getReadingOrder() - b.getReadingOrder() : compared;
                 }
                );

        return surroundingEnermies.isPresent() ?
            surroundingEnermies.get() : null;
    }
}
