package aoc2018.day15;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import lombok.*;

class Navigator {
    private final CombatMap combatMap;

    private static final int MAX_DISTANCE = 9999999;

    Navigator(CombatMap combatMap) {
        this.combatMap = combatMap;
    }

    Point moveTarget(Point from) {
        val inRangePoints = findInRangePointsFor(from);

        val distances = calculateMapDistance(from, null);

        val reachablePoints = inRangePoints.stream()
            .filter(p -> distances.get(p) != MAX_DISTANCE)
            .collect(Collectors.toList());

        if (reachablePoints.isEmpty()) return null;

        val minDistance = reachablePoints.stream()
            .mapToInt(p -> distances.get(p))
            .min()
            .getAsInt();

        val candidatePoints = new ArrayList<Point>();
        reachablePoints.stream()
                .filter(p -> distances.get(p) == minDistance)
                .forEach(p -> {
                    candidatePoints.add(findShortestPaths(from, p).get(1));
                });

        return candidatePoints.stream()
            .min(Comparator.comparingInt(Point::getReadingOrder))
            .get();
    }

    List<Point> reachablePoints(Point from) {
        val reachablePoints = new ArrayList<Point>();
        val inRangePoints = findInRangePointsFor(from);

        for (var to : inRangePoints) {
            if (isReachable(from, to)) {
                reachablePoints.add(to);
            }
        }

        return reachablePoints;
    }

    boolean isReachable(Point from, Point to) {
        return isReachable(from, to, new LinkedList<Point>());
    }

    boolean isReachable(Point from, Point to, Deque<Point> previousPath) {
        if (combatMap.getSurroundingPoints(from).anyMatch(p -> p == to)) return true;

        val surroundingPoints = combatMap.getSurroundingPoints(from);

        previousPath.push(from);

        return surroundingPoints
            .filter(p -> p.isSpace())
            .filter(p -> !previousPath.stream().anyMatch(seen -> seen == p))
            .anyMatch(point -> isReachable(point, to, previousPath));
    }

    Map<Point, Integer> calculateMapDistance(Point source, Point target) {
        val pointSet = new HashSet<Point>();
        val distances = new HashMap<Point, Integer>();

        int inf = MAX_DISTANCE;

        for (int r = 0; r < combatMap.getRowSize(); r++) {
            for (int c = 0; c < combatMap.getColumnSize(); c++) {
                val point = combatMap.getPoint(r, c);
                pointSet.add(point);
                distances.put(point, inf);
            }
        }

        distances.put(source, 0);

        // color distance
        while(!pointSet.isEmpty()) {
            val minDistance = pointSet.stream()
                .mapToLong(p -> distances.get(p))
                .min()
                .getAsLong();

            val u = pointSet.stream()
                .filter(p -> distances.get(p) == minDistance)
                .findFirst()
                .get();

            pointSet.remove(u);

            if (nonNull(target) && distances.get(u) >= distances.get(target))
                break;

            combatMap.getSurroundingPoints(u)
                .filter(p -> p.isSpace())
                .forEach(n -> {
                    val alt = distances.get(u) + 1;
                    if (alt < distances.get(n)) {
                        distances.put(n, alt);
                    }
                });
        }

        return distances;
    }

    List<Point> findShortestPaths(Point source, Point target) {
        return findShortestPaths(source, target, null);
    }

    List<Point> findShortestPaths(Point source, Point target, Map<Point, Integer> distances) {
        if (isNull(distances)) distances = calculateMapDistance(source, target);

        val paths = findPathUtilTarget(source, target, distances);

        return paths.stream()
            .min((a, b) -> {
                if (a.size() == b.size()) {
                    return a.get(1).getReadingOrder() - b.get(1).getReadingOrder();
                }
                return a.size() - b.size();
                })
            .get();
    }

    List<List<Point>> findPathUtilTarget(Point source, Point target,
                                         Map<Point, Integer> distances) {
        val nextPoints = combatMap.getSurroundingPoints(source)
            .filter(p -> p.isSpace())
            .filter(p -> distances.get(p) > distances.get(source))
            .collect(Collectors.toList());

        val paths = new ArrayList<List<Point>>();
        val sourceDistance = distances.get(source);

        for (val point : nextPoints) {
            val pointDistance = distances.get(point);

            if (point == target) {
                val path = new ArrayList<Point>();
                path.add(source);
                path.add(target);
                paths.add(path);
            } else {
                val subpaths = findPathUtilTarget(point, target, distances);

                for (val sp : subpaths) {
                    val path = new ArrayList<Point>();
                    path.add(source);
                    path.addAll(sp);
                    paths.add(path);
                }
            }
        }

        return paths;
    }

    List<Point> findInRangePointsFor(Point point) {
        val list = new ArrayList<Point>();
        if (!point.isUnit()) return list;

        val unit = point.getUnit();

        for (var i = 0; i < combatMap.getRowSize(); i++) {
            for (var j = 0; j < combatMap.getColumnSize(); j++) {
                val otherPoint = combatMap.getPoint(i, j);

                if (isNull(otherPoint) || !otherPoint.isUnit()) continue;

                if (otherPoint.getUnit().isFriend(unit)) continue;

                combatMap.getSurroundingPoints(otherPoint).filter(p -> p.isSpace())
                    .forEach(list::add);
            }
        }

        return list;
    }
}
