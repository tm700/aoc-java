package aoc2018.day15;

import java.util.*;
import java.util.stream.*;
import java.util.function.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import lombok.*;

public class CombatMap {
    @Getter private int rowSize;
    @Getter private int columnSize;

    private Point[][] map;

    public static CombatMap parseCombatMap(List<String> lines) {
        val combatMap = new CombatMap(lines.size(), lines.get(0).length());

        for (var r = 0; r < lines.size(); r++) {
            val line = lines.get(r);
            for (var c = 0; c < line.length(); c++) {
                combatMap.setPoint(r, c, line.charAt(c));
            }
        }

        return combatMap;
    }

    public static CombatMap parseCombatMap(String[] lines) {
        val combatMap = new CombatMap(lines.length, lines[0].length());

        for (var r = 0; r < lines.length; r++) {
            val line = lines[r];
            for (var c = 0; c < line.length(); c++) {
                combatMap.setPoint(r, c, line.charAt(c));
            }
        }

        return combatMap;
    }

    Point getPoint(int row, int column) {
        if (row < 0 || row >= rowSize) return null;
        if (column < 0 || column >= columnSize) return null;

        return map[row][column];
    }

    Stream<Point> getSurroundingPoints(Point point) {
        val r = point.getRow();
        val c = point.getColumn();

        return Arrays.stream(new Point[]{
                getPoint(r - 1, c),
                getPoint(r, c - 1), getPoint(r, c + 1),
                getPoint(r + 1, c)
            }).filter(p -> p != null);
    }

    void movePoint(Point from, Point to) {
        if (!from.isUnit())
            throw new IllegalArgumentException("from is not a unit");

        if (!to.isSpace())
            throw new IllegalArgumentException(from + " to is not space: " + to);

        val unit = from.getUnit();
        to.setUnit(unit);
        from.setSpace();
    }

    public String[] displayMap() {
        val map = new String[rowSize];

        for (var i = 0; i < rowSize; i++) {
            val b = new StringBuilder();
            for (var j = 0; j < columnSize; j++) {
                val point = getPoint(i, j);
                b.append(point.getSymbol());
            }
            map[i] = b.toString();
        }

        return map;
    }

    Point placeUnit(Unit unit, int row, int column) {
        val point = getPoint(row, column);
        if (!point.isSpace()) {
            throw new IllegalArgumentException(
                String.format("Point (%s, %s) must be space to place a unit",
                              row, column)
            );
        }

        point.setUnit(unit);

        return point;
    }

    void clearPoint(int row, int column) {
        val point = getPoint(row, column);
        if (isNull(point)) return;
        point.setSpace();
    }

    private CombatMap(int rowSize, int columnSize) {
        this.rowSize    = rowSize;
        this.columnSize = columnSize;
        this.map        = new Point[rowSize][columnSize];

        for (var i = 0; i < rowSize; i++) {
            for (var j = 0; j < columnSize; j++) {
                this.map[i][j] = new Point(i, j, i * columnSize + j);
            }
        }
    }

    private void setPoint(int row, int column, char symbol) {
        if (symbol == Point.SPACE) {
            return;
        } else if (symbol == Point.WALL) {
            this.map[row][column].setWall();
        } else {
            this.placeUnit(new Unit(row, column, symbol), row, column);
        }
    }
}
