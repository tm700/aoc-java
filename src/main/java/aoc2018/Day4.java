package aoc2018;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Pattern;
import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

import lombok.val;

class Day4 extends Solver {
    private static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private static class PatrolLog {
        private static final String BEGIN  = "begins shift";
        private static final String SLEEP  = "falls asleep";
        private static final String WAKEUP = "wakes up";

        private final int guardId;
        private final Calendar datetime;
        private final String action;

        private PatrolLog(int guardId, String datetime, String action) {
            this.guardId  = guardId;
            this.datetime = Calendar.getInstance();

            try {
                this.datetime.setTime(DATETIME_FORMAT.parse(datetime));
            } catch(Exception e) {
                throw new IllegalArgumentException("wrong datetime format");
            }

            this.action   = action;
        }

        private boolean begin() {
            return action.equals(BEGIN);
        }

        private boolean sleep() {
            return action.equals(SLEEP);
        }

        @Override
        public String toString() {
            return guardId + " " + datetime + " " + action;
        }
    }

    private final List<PatrolLog> patrolLogs;

    private final Pattern LINE_PATTERN = Pattern
        .compile(
            "\\[(\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2})\\] (Guard )?#?(\\d+)? ?(\\w+ \\w+)"
        );

    Day4(String file) {
        setDataFile(file);
        this.patrolLogs = parsePatrolLogs();
    }

    long call1() {
        val guardId = laziestGuard();

        val mostLikelySleepMinute = guardSleepMinuteCounts(guardId)
            .entrySet()
            .stream()
            .max(Entry.comparingByValue())
            .get()
            .getKey();

        return guardId * mostLikelySleepMinute;
    }

    long call2() {
        //
        int maxGuardId = 0;
        int maxMinutes = 0;
        int maxTimes = 0;

        for (val guardId : sleepMinuteCounts().keySet()) {
            val minuteCount = guardSleepMinuteCounts(guardId)
                .entrySet()
                .stream()
                .max(Entry.comparingByValue())
                .get();

            if (minuteCount.getValue() > maxTimes) {
                maxGuardId = guardId;
                maxMinutes = minuteCount.getKey();
                maxTimes = minuteCount.getValue();
            }
        }

        return maxMinutes * maxGuardId;
    }

    private int laziestGuard() {
        val sleepCounts = sleepMinuteCounts();

        val laziest = sleepCounts.entrySet()
            .stream()
            .max(Entry.comparingByValue())
            .get();

        return laziest.getKey();
    }

    private Map<Integer, Integer> guardSleepMinuteCounts(Integer guardId) {
        val sleepPatterns = sortSleepPatterns();

        val minuteCounts = new HashMap<Integer, Integer>();

        for (val pattern : sleepPatterns.values()) {
            if (!pattern.guardSleepMinutes.containsKey(guardId)) continue;

            for (val min : pattern.guardSleepMinutes.get(guardId)) {
                minuteCounts.putIfAbsent(min, 0);
                minuteCounts.put(min, minuteCounts.get(min) + 1);
            }
        }

        return minuteCounts;
    }

    private static class GuardDailySleepPattern {
        private final Map<Integer, List<Integer>> guardSleepMinutes = new HashMap<>();

        private void addSleepMinute(int guardId, int minute) {
            guardSleepMinutes.putIfAbsent(guardId, new ArrayList<Integer>());

            guardSleepMinutes.get(guardId)
                .add(minute);
        }
    }

    private List<PatrolLog> parsePatrolLogs() {
        val patrolLogs = new ArrayList<PatrolLog>();

        int currentGuardId = -1;

        val lines = getLines();
        Collections.sort(lines);

        for (val line : lines) {
            val matcher = LINE_PATTERN.matcher(line);

            if (matcher.find()) {
                val datetime = matcher.group(1);
                val guardId  = matcher.group(3);
                val action   = matcher.group(4);

                if (nonNull(guardId))
                    currentGuardId = Integer.parseInt(guardId);

                patrolLogs.add(new PatrolLog(currentGuardId, datetime, action));
            } else {
                throw new IllegalArgumentException("wrong line format");
            }
        }

        return patrolLogs;
    }

    private Map<Integer, Integer> sleepMinutesCounts;
    private Map<Integer, Integer> sleepMinuteCounts() {
        val sleepPatterns = sortSleepPatterns();
        if (nonNull(sleepMinutesCounts)) return sleepMinutesCounts;

        sleepMinutesCounts = new HashMap<Integer, Integer>();

        for (val pattern : sleepPatterns.values()) {
            for (val guardSleepMinutes : pattern.guardSleepMinutes.entrySet()) {
                val guardId = guardSleepMinutes.getKey();
                sleepMinutesCounts.putIfAbsent(guardId, 0);

                sleepMinutesCounts.put(
                    guardId,
                    sleepMinutesCounts.get(guardId) + guardSleepMinutes.getValue().size()
                );
            }
        }

        return sleepMinutesCounts;
    }

    private Map<String, GuardDailySleepPattern> dailySleepPatternMap;

    private Map<String, GuardDailySleepPattern> sortSleepPatterns() {
        if (nonNull(dailySleepPatternMap)) return dailySleepPatternMap;

        val shifts = sortPatrolLogsToShifts();

        dailySleepPatternMap = new HashMap<String, GuardDailySleepPattern>();

        for (val shift : shifts) {
            PatrolLog fallsAsleepLog = null;

            for (val log : shift) {
                if (log.sleep()) {
                    fallsAsleepLog = log;
                    continue;
                }

                if (isNull(fallsAsleepLog)) continue;

                val sleepTime = (Calendar) fallsAsleepLog.datetime.clone();
                val guardId = fallsAsleepLog.guardId;

                while (sleepTime.compareTo(log.datetime) < 0) {
                    String date = DATE_FORMAT.format(sleepTime.getTime());

                    dailySleepPatternMap.putIfAbsent(date,
                                                     new GuardDailySleepPattern());
                    dailySleepPatternMap.get(date)
                        .addSleepMinute(guardId,
                                        sleepTime.get(Calendar.MINUTE));
                    sleepTime.add(Calendar.SECOND, 60);
                }
            }
        }

        return dailySleepPatternMap;
    }

    private List<List<PatrolLog>> shifts;
    private List<List<PatrolLog>> sortPatrolLogsToShifts() {
        if (nonNull(shifts)) return shifts;

        shifts = new ArrayList<List<PatrolLog>>();

        for (val log : patrolLogs) {
            // new shift
            if (log.begin())
                shifts.add(new ArrayList<PatrolLog>());

            if (shifts.size() == 0) {
                throw new IllegalArgumentException(log + " has no begin");
            }
            // Add the log to the current shift
            shifts.get(shifts.size() - 1)
                .add(log);
        }

        return shifts;
    }
}
