package aoc2018;

import java.util.*;
import lombok.val;

class Day1 extends Solver {
    Day1(String file) {
        setDataFile(file);
    }

    int call1() {
        return getAsIntStream().sum();
    }

    int call2() {
        val changes = getAsIntStream().toArray();
        var current = 0;
        val seen = new HashSet<>();
        seen.add(current);

        do  {
            for (val i : changes) {
                current += i;
                if (seen.contains(current)) return current;
                seen.add(current);
            }
        } while(true);
    }
}
