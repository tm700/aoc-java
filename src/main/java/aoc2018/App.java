package aoc2018;

import lombok.*;

public class App {
    public String getGreeting() {
        return "Hello world.";
    }

    public static void main(String[] args) {
        System.out.println(new App().getGreeting());

        val s = new Day15("data/day15_sample_rb.txt");

        var doRound = true;

        while (doRound) {
            val points = s.getControl().getPoints();
            doRound = s.getControl().doRound();

            // s.getControl().printUnitPoints();
        }

        val round = s.getControl().getRound();

        val hitPoints = s.getControl().getTotalHitPoints();
        System.err.println("after round: " + round
                           + " " + hitPoints
                           + " " + (round - 1) * hitPoints
            );
    }
}
