package aoc2018;

import java.util.List;
import java.util.stream.IntStream;

class Solver {
    private String dataFile;
    private DataLoader loader;

    protected void setDataFile(String file) {
        this.dataFile = file;
        this.loader = new DataLoader(file);
    }

    protected List<String> getLines() {
        return this.loader.getLines();
    }

    protected IntStream getAsIntStream() {
        return getLines()
            .stream()
            .mapToInt((i) -> Integer.parseInt(i));
    }
}
