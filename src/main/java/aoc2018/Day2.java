package aoc2018;

import java.util.*;
import java.util.stream.Stream;
import java.util.stream.IntStream;

import lombok.val;


class Day2 extends Solver {
    Day2(String file) {
        setDataFile(file);
    }

    public int call1() {
        val times = new Integer[]{2, 3};
        val counter = makeLineCount(times);

        for (val line : getLines()) {
            val charCounters = new HashMap<Character, Integer>();
            val lineCounter = makeLineCount(times);

            for (val c : line.toCharArray())
                charCounters.put(c, charCounters.getOrDefault(c, 0) + 1);

            charCounters.values()
                .stream()
                .forEach(count -> Stream.of(times)
                         .filter(i -> count == i)
                         .forEach(i -> lineCounter.put(i, 1)));

            Stream
                .of(times)
                .forEach(i -> counter.put(i, lineCounter.get(i) + counter.get(i)));
        }

        return counter.values().stream().reduce((x, y) -> x * y).get();
    }

    public String call2() {
        val pair = Objects.requireNonNull(findPair());
        val b = new StringBuilder();

        IntStream.range(0, pair[0].length())
            .filter(i -> pair[0].charAt(i) == pair[1].charAt(i))
            .forEach(i -> b.append(pair[0].charAt(i)));

        return b.toString();
    }

    private Map<Integer, Integer> makeLineCount(Integer[] times) {
        val lineCounter = new HashMap<Integer, Integer>();
        Stream.of(times).forEach(i -> lineCounter.put(i, 0));
        return lineCounter;
    }

    private String[] findPair() {
        val lines = getLines();
        for (val source : lines) {
            val pair = findPairDiffByOne(source, lines);
            if (pair != null) return pair;
        }

        return null;
    }

    private String[] findPairDiffByOne(String source, List<String> lines) {
        for (val target : lines) {
            var diffCount = 0;

            for (int i = 0; i < source.length(); i++) {
                if (source.charAt(i) != target.charAt(i) && ++diffCount > 1) break;
            }

            if (diffCount == 1) return new String[]{source, target};
        }

        return null;
    }
}
