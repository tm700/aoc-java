package aoc2018;

import java.util.ArrayList;
import java.util.List;
import java.io.*;

/**
 *
 * @author marty.ma
 */
class DataLoader {
    private final String dataFile;
    private List<String> data;

    DataLoader(String dataFile) {
        this.dataFile = dataFile;
    }

    public List<String> getLines() {
        if (data == null) data = readLines();

        return data;
    }

    private List<String> readLines() {
        data = new ArrayList<>();
        String line;

        try (FileReader fr = new FileReader(new File(dataFile));
             BufferedReader lineReader = new BufferedReader(fr); ) {
            while((line = lineReader.readLine()) != null) {
                line = line.trim();
                if (!line.isEmpty()) data.add(line);
            }
        } catch (IOException e) {
            throw new RuntimeException(String.format("Test file (%s) not found", dataFile));
        }

        return data;
    }
}
