package aoc2018.day15;

import java.io.*;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.junit.Assert.*;

import lombok.*;

public class CombatMapTests {

    // Targets:      In range:     Reachable:    Nearest:      Chosen:
    // #######       #######       #######       #######       #######
    // #E..G.#       #E.?G?#       #E.@G.#       #E.!G.#       #E.+G.#
    // #...#.#  -->  #.?.#?#  -->  #.@.#.#  -->  #.!.#.#  -->  #...#.#
    // #.G.#G#       #?G?#G#       #@G@#G#       #!G.#G#       #.G.#G#
    // #######       #######       #######       #######       #######

    @Test public void canFindInRangePoints() {
        val lines = new String[]{
            "#######",
            "#E..G.#",
            "#...#.#",
            "#.G.#G#",
            "#######"
        };

        val combatMap = CombatMap.parseCombatMap(lines);
        val nav = new Navigator(combatMap);

        assertNotNull(combatMap);

        val point = combatMap.getPoint(1, 1);

        val inRangePoints = nav.findInRangePointsFor(point);

        for (var co : new int[][]{{1, 3}, {1, 5}, {2, 2}, {2, 5}, {3, 1}, {3, 3}}) {
            var otherPoint = combatMap.getPoint(co[0], co[1]);

            assertTrue(inRangePoints.stream().filter(p -> p == otherPoint)
                       .findFirst()
                       .isPresent());
        }
    }

    // Targets:      In range:     Reachable:    Nearest:      Chosen:
    // #######       #######       #######       #######       #######
    // #E..G.#       #E.?G?#       #E.@G.#       #E.!G.#       #E.+G.#
    // #...#.#  -->  #.?.#?#  -->  #.@.#.#  -->  #.!.#.#  -->  #...#.#
    // #.G.#G#       #?G?#G#       #@G@#G#       #!G.#G#       #.G.#G#
    // #######       #######       #######       #######       #######

    // @Test public void canFindReachablePoints() {
    //     val lines = new String[] {
    //         "#######",
    //         "#E..G.#",
    //         "#...#.#",
    //         "#.G.#G#",
    //         "#######"
    //     };

    //     val combatMap = CombatMap.parseCombatMap(lines);

    //     val point = combatMap.getPoint(1, 1);

    //     val nav = new Navigator(combatMap);

    //     val inRangePoints = nav.findInRangePointsFor(point);

    //     assertEquals(6, inRangePoints.size());

    //     val points = nav.reachablePoints(point);

    //     assertEquals(4, points.size());
    // }

    @Test public void testMove() {
        val lines = new String[]{
            "#########",
            "#G..G..G#",
            "#.......#",
            "#.......#",
            "#G..E..G#",
            "#.......#",
            "#.......#",
            "#G..G..G#",
            "#########"
        };

        val combatMap = CombatMap.parseCombatMap(lines);
        val nav = new Navigator(combatMap);

        var g = combatMap.getPoint(1, 1);
        var t = nav.moveTarget(g);

        assertEquals(t, combatMap.getPoint(1, 2));

        combatMap.movePoint(g, t);

        g = combatMap.getPoint(1, 4);
        t = nav.moveTarget(g);

        assertEquals(t, combatMap.getPoint(2, 4));

        combatMap.movePoint(g, t);

        g = combatMap.getPoint(1, 7);
        t = nav.moveTarget(g);

        assertEquals(t, combatMap.getPoint(1, 6));
        combatMap.movePoint(g, t);

        g = combatMap.getPoint(4, 1);
        t = nav.moveTarget(g);
        assertEquals(t, combatMap.getPoint(4, 2));

        combatMap.movePoint(g, t);

        g = combatMap.getPoint(4, 4);

        // val chosenPoint = nav.chooseMovePoint(g);
    }
}
