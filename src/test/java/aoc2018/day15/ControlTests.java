package aoc2018.day15;

import java.io.*;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import static org.junit.Assert.*;

import lombok.*;

public class ControlTests {
    // Targets:      In range:     Reachable:    Nearest:      Chosen:
    // #######       #######       #######       #######       #######
    // #E..G.#       #E.?G?#       #E.@G.#       #E.!G.#       #E.+G.#
    // #...#.#  -->  #.?.#?#  -->  #.@.#.#  -->  #.!.#.#  -->  #...#.#
    // #.G.#G#       #?G?#G#       #@G@#G#       #!G.#G#       #.G.#G#
    // #######       #######       #######       #######       #######
    @Test public void noBattleWhenNoEnemies() {
        val lines = new String[] {
            "#######",
            "#G..G.#",
            "#...#.#",
            "#.G.#G#",
            "#######"
        };

        val combatMap = CombatMap.parseCombatMap(lines);
        val control = new Control(combatMap);

        control.run();

        assertEquals(control.getRound(), 0);
    }

    @Test
    public void testMovements() {
        val lines = new String[]{
            "#########",
            "#G..G..G#",
            "#.......#",
            "#.......#",
            "#G..E..G#",
            "#.......#",
            "#.......#",
            "#G..G..G#",
            "#########"
        };

        val combatMap = CombatMap.parseCombatMap(lines);
        val control = new Control(combatMap);

        control.doRound();

        var newLines = new String[]{
            "#########",
            "#.G...G.#",
            "#...G...#",
            "#...E..G#",
            "#.G.....#",
            "#.......#",
            "#G..G..G#",
            "#.......#",
            "#########"
        };

        assertArrayEquals(combatMap.displayMap(), newLines);

        newLines = new String[]{
            "#########",
            "#..G.G..#",
            "#...G...#",
            "#.G.E.G.#",
            "#.......#",
            "#G..G..G#",
            "#.......#",
            "#.......#",
            "#########"
        };

        control.doRound();
        assertArrayEquals(combatMap.displayMap(), newLines);

        newLines = new String[]{
            "#########",
            "#.......#",
            "#..GGG..#",
            "#..GEG..#",
            "#G..G...#",
            "#......G#",
            "#.......#",
            "#.......#",
            "#########"
        };

        control.doRound();
        assertArrayEquals(combatMap.displayMap(), newLines);

        assertEquals(control.getRound(), 3);
    }

    @Test
    public void testAttacks() {
        // #######
        // #.G...#   G(200)
        // #...EG#   E(200), G(200)
        // #.#.#G#   G(200)
        // #..G#E#   G(200), E(200)
        // #.....#
        // #######

        val lines = new String[]{
            "#######",
            "#.G...#",
            "#...EG#",
            "#.#.#G#",
            "#..G#E#",
            "#.....#",
            "#######"
        };
        val combatMap = CombatMap.parseCombatMap(lines);
        val control = new Control(combatMap);

        control.doRound();

        var newMap = new String[]{
            "#######",
            "#..G..#",
            "#...EG#",
            "#.#G#G#",
            "#...#E#",
            "#.....#",
            "#######"
        };

        assertArrayEquals(combatMap.displayMap(), newMap);

        val point = combatMap.getPoint(2, 4);
        assertEquals(point.getUnit().isElf(), true);
        assertEquals(point.getUnit().getHitPoints(), 197);
    }

    @Test
    public void testRun() {
        // #######
        // #.G...#   G(200)
        // #...EG#   E(200), G(200)
        // #.#.#G#   G(200)
        // #..G#E#   G(200), E(200)
        // #.....#
        // #######

        val lines = new String[]{
            "#######",
            "#.G...#",
            "#...EG#",
            "#.#.#G#",
            "#..G#E#",
            "#.....#",
            "#######"
        };
        val combatMap = CombatMap.parseCombatMap(lines);
        val control = new Control(combatMap);

        control.run();
        var point = combatMap.getPoint(3, 5);
        assertEquals(point.getUnit().getHitPoints(), 59);
        assertEquals(control.getRound(), 47);
        assertEquals(control.getTotalHitPoints(), 590);
    }

    @Test
    public void testRun2() {
        // #######       #######
        // #G..#E#       #...#E#   E(200)
        // #E#E.E#       #E#...#   E(197)
        // #G.##.#  -->  #.E##.#   E(185)
        // #...#E#       #E..#E#   E(200), E(200)
        // #...E.#       #.....#
        // #######       #######

        val lines = new String[]{
            "#######",
            "#G..#E#",
            "#E#E.E#",
            "#G.##.#",
            "#...#E#",
            "#...E.#",
            "#######"
        };

        val combatMap = CombatMap.parseCombatMap(lines);
        val control = new Control(combatMap);

        control.run();

        assertEquals(control.getRound() - 1, 37);
        assertEquals(control.getTotalHitPoints(), 982);
    }

    @Test

    public void testMove() {
        val lines = new String[]{
            "######",
            "#.G..#",
            "#...E#",
            "#E...#",
            "######"
        };

        val combatMap = CombatMap.parseCombatMap(lines);
        val nav = new Navigator(combatMap);

        var g = combatMap.getPoint(1, 2);
        var t = nav.moveTarget(g);

        assertEquals(t, combatMap.getPoint(1, 1));
    }

    @Test
    public void testRun20RoundsSample() {
        val lines = new String[]{
            "#########",
            "#G......#",
            "#.E.#...#",
            "#..##..G#",
            "#...##..#",
            "#...#...#",
            "#.G...G.#",
            "#.....G.#",
            "#########"
        };

        val combatMap = CombatMap.parseCombatMap(lines);
        val control = new Control(combatMap);

        control.run();

        assertEquals(control.getRound() - 1, 20);
        assertEquals(control.getTotalHitPoints(), 937);
    }
}
