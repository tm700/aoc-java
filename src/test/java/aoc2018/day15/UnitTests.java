package aoc2018.day15;

import org.junit.Test;
import static org.junit.Assert.*;

public class UnitTests {
    @Test public void canCreateUnit() {
        var unit = new Unit(0, 0, Point.GOBLIN);
        assertTrue(unit.isGoblin());

        var unit2 = new Unit(0, 0, Point.ELF);
        assertTrue(unit2.isElf());

        assertTrue(unit2.isEnemy(unit));
        assertTrue(unit2.isFriend(unit2));
    }
}
